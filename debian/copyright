Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: CrackMapExec
Source: https://github.com/byt3bl33d3r/crackmapexec

Files: *
Copyright: 2015-2019, byt3bl33d3r
License: BSD-2-clause

Files: cme/msfrpc.py
Copyright: 2014-2016 ryan linn - rlinn@trustwave.com, marcello salvati - byt3bl33d3r@gmail.com
License: GPL-3+

Files: cme/data/invoke-obfuscation/*
Copyright: 2017 daniel bohannon <@danielhbohannon>
License: Apache-2.0

Files: cme/data/netripper/NetRipper/ReflectiveDLLInjection.h
 cme/data/invoke-vnc/ReflectiveDLLInjection/*
Copyright: 2012 stephen fewer of harmony security (www.harmonysecurity.com)
License: BSD-3-clause

Files: cme/data/netripper/minhook/*
Copyright: 2009-2017 tsuda kageyu
License: BSD-2-clause

Files: cme/data/powersploit/Exfiltration/NTFSParser/*
Copyright: 2010 cyb70289 <cyb70289@gmail.com>
License: GPL-2+

Files: cme/data/invoke-vnc/*
Copyright: 1999 at&t laboratories cambridge
License: GPL-2+

Files: cme/data/invoke-vnc/winvnc/zlib/*
Copyright: 1995-2002 jean-loup gailly
License: Special
 1995-2002 jean-loup gailly and mark adler
 .
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. if you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

Files: cme/data/invoke-vnc/winvnc/libjpeg/*
Copyright: 1994-1997 thomas g. lane
License: Other

Files: cme/data/invoke-vnc/winvnc/libjpeg/ansi2knr.c
Copyright: 1988 richard m. stallman
License: GPL-2+

Files: cme/data/mimipenguin/*
Copyright: huntergregal
License: CC-BY-4.0

Files: debian/*
Copyright: 2017-2020 sophie brun <sophie@offensive-security.com>
License: BSD-2-clause

License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either version 2 of
 the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: GPL-3+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
       http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the full text of the Apache License, Version 2.0
 can be found in the file `/usr/share/common-licenses/Apache-2.0'.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 1. Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
 derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: Other
 In plain english:
 .
 1. We don't promise that this software works. (but if you find any bugs,
    please let us know!)
 2. You can use this software for whatever you want. You don't have to pay us.
 3. You may not pretend that you wrote this software. If you use it in a
    program, you must acknowledge somewhere in your documentation that
    you've used the ijg code.
 .
 In legalese:
 .
 The authors make no warranty or representation, either express or implied,
 with respect to this software, its quality, accuracy, merchantability, or
 fitness for a particular purpose. This software is provided "as is", and you,
 its user, assume the entire risk as to its quality and accuracy.
 .
 This software is copyright (c) 1991-1998, thomas g. lane.
 All rights reserved except as specified below.
 .
 Permission is hereby granted to use, copy, modify, and distribute this
 software (or portions thereof) for any purpose, without fee, subject to these
 conditions:
 (1) if any part of the source code for this software is distributed, then this
 readme file must be included, with this copyright and no-warranty notice
 unaltered; and any additions, deletions, or changes to the original files
 must be clearly indicated in accompanying documentation.
 (2) if only executable code is distributed, then the accompanying
 documentation must state that "this software is based in part on the work of
 the independent jpeg group".
 (3) permission for use of this software is granted only if the user accepts
 full responsibility for any undesirable consequences; the authors accept
 no liability for damages of any kind.
 .
 These conditions apply to any software derived from or based on the ijg code,
 not just to the unmodified library.  if you use our work, you ought to
 acknowledge us.
 .
 Permission is not granted for the use of any ijg author's name or company name
 in advertising or publicity relating to this software or products derived from
 it. This software may be referred to only as "the independent jpeg group's
 software".
 .
 We specifically permit and encourage the use of this software as the basis of
 commercial products, provided that all warranty or liability claims are
 assumed by the product vendor.

License: BSD-2-clause
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 * Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 . 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: CC-BY-4.0
 By exercising the licensed rights (defined below), you accept and agree
 to be bound by the terms and conditions of this creative commons
 attribution 4.0 international public license ("public license"). To the
 extent this public license may be interpreted as a contract, you are
 granted the licensed rights in consideration of your acceptance of these
 terms and conditions, and the licensor grants you such rights in
 consideration of benefits the licensor receives from making the licensed
 material available under these terms and conditions.
 .
 Section 1 – Definitions.
 .
   a.adapted material means material subject to copyright and similar
   rights that is derived from or based upon the licensed material and in
   which the licensed material is translated, altered, arranged,
   transformed, or otherwise modified in a manner requiring permission
   under the copyright and similar rights held by the licensor. for
   purposes of this public license, where the licensed material is a
   musical work, performance, or sound recording, adapted material is
   always produced where the licensed material is synched in timed
   relation with a moving image.
   b.adapter's license means the license you apply to your copyright and
   similar rights in your contributions to adapted material in accordance
   with the terms and conditions of this public license.
   c.copyright and similar rights means copyright and/or similar rights
   closely related to copyright including, without limitation,
   performance, broadcast, sound recording, and sui generis database
   rights, without regard to how the rights are labeled or categorized.
   for purposes of this public license, the rights specified in section
   2(b)(1)-(2) are not copyright and similar rights.
   d.effective technological measures means those measures that, in the
   absence of proper authority, may not be circumvented under laws
   fulfilling obligations under article 11 of the wipo copyright treaty
   adopted on december 20, 1996, and/or similar international agreements.
   e.exceptions and limitations means fair use, fair dealing, and/or any
   other exception or limitation to copyright and similar rights that
   applies to your use of the licensed material.
   f.licensed material means the artistic or literary work, database, or
   other material to which the licensor applied this public license.
   g.licensed rights means the rights granted to you subject to the terms
   and conditions of this public license, which are limited to all
   copyright and similar rights that apply to your use of the licensed
   material and that the licensor has authority to license.
   h.licensor means the individual(s) or entity(ies) granting rights
   under this public license.
   i.share means to provide material to the public by any means or
   process that requires permission under the licensed rights, such as
   reproduction, public display, public performance, distribution,
   dissemination, communication, or importation, and to make material
   available to the public including in ways that members of the public
   may access the material from a place and at a time individually chosen
   by them.
   j.sui generis database rights means rights other than copyright
   resulting from directive 96/9/ec of the european parliament and of the
   council of 11 march 1996 on the legal protection of databases, as
   amended and/or succeeded, as well as other essentially equivalent
   rights anywhere in the world.
   k.you means the individual or entity exercising the licensed rights
   under this public license. your has a corresponding meaning.
 .
 Section 2 – Scope.
 .
    a.License grant.
        1.subject to the terms and conditions of this public license, the
        licensor hereby grants you a worldwide, royalty-free,
        non-sublicensable, non-exclusive, irrevocable license to exercise
        the licensed rights in the licensed material to:
            a.reproduce and share the licensed material, in whole or in
            part; and
            b.produce, reproduce, and share adapted material.
        2.exceptions and limitations. for the avoidance of doubt, where
        exceptions and limitations apply to your use, this public license
        does not apply, and you do not need to comply with its terms and
        conditions.
        3.term. the term of this public license is specified in section
        6(a).
    4.media and formats; technical modifications allowed. the licensor
    authorizes you to exercise the licensed rights in all media and
    formats whether now known or hereafter created, and to make
    technical modifications necessary to do so. the licensor waives
    and/or agrees not to assert any right or authority to forbid you
    from making technical modifications necessary to exercise the
    licensed rights, including technical modifications necessary to
    circumvent effective technological measures. for purposes of this
    public license, simply making modifications authorized by this
    section 2(a)(4) never produces adapted material.
        5.downstream recipients.
        a.offer from the licensor – licensed material. every recipient
        of the licensed material automatically receives an offer from
        the licensor to exercise the licensed rights under the terms
        and conditions of this public license.
        b.no downstream restrictions. you may not offer or impose any
        additional or different terms or conditions on, or apply any
        effective technological measures to, the licensed material if
        doing so restricts exercise of the licensed rights by any
        recipient of the licensed material.
    6.no endorsement. nothing in this public license constitutes or
    may be construed as permission to assert or imply that you are, or
    that your use of the licensed material is, connected with, or
    sponsored, endorsed, or granted official status by, the licensor
    or others designated to receive attribution as provided in section
    3(a)(1)(a)(i).
 .
    b.Other rights.
    1.moral rights, such as the right of integrity, are not licensed
    under this public license, nor are publicity, privacy, and/or
    other similar personality rights; however, to the extent possible,
    the licensor waives and/or agrees not to assert any such rights
    held by the licensor to the limited extent necessary to allow you
    to exercise the licensed rights, but not otherwise.
    2.patent and trademark rights are not licensed under this public
    license.
    3.to the extent possible, the licensor waives any right to collect
    royalties from you for the exercise of the licensed rights,
    whether directly or through a collecting society under any
    voluntary or waivable statutory or compulsory licensing scheme. in
    all other cases the licensor expressly reserves any right to
    collect such royalties.
 .
 Section 3 – License conditions.
 .
 Your exercise of the licensed rights is expressly made subject to the following conditions.
 .
    a.attribution.
 .
    1.if you share the licensed material (including in modified form),
    you must:
        a.retain the following if it is supplied by the licensor with
        the licensed material:
        i.identification of the creator(s) of the licensed
        material and any others designated to receive attribution,
        in any reasonable manner requested by the licensor
        (including by pseudonym if designated);
                ii.a copyright notice;
                iii.a notice that refers to this public license;
                iv.a notice that refers to the disclaimer of warranties;
                v.a uri or hyperlink to the licensed material to the extent reasonably practicable;
        b.indicate if you modified the licensed material and retain an
        indication of any previous modifications; and
        c.indicate the licensed material is licensed under this public
        license, and include the text of, or the uri or hyperlink to,
        this public license.
    2.you may satisfy the conditions in section 3(a)(1) in any
    reasonable manner based on the medium, means, and context in which
    you share the licensed material. for example, it may be reasonable
    to satisfy the conditions by providing a uri or hyperlink to a
    resource that includes the required information.
    3.if requested by the licensor, you must remove any of the
    information required by section 3(a)(1)(a) to the extent
    reasonably practicable.
    4.if you share adapted material you produce, the adapter's license
    you apply must not prevent recipients of the adapted material from
    complying with this public license.
 .
 Section 4 – Sui generis database rights.
 .
 where the licensed rights include sui generis database rights that apply
 to your use of the licensed material:
 .
    a.for the avoidance of doubt, section 2(a)(1) grants you the right to
    extract, reuse, reproduce, and share all or a substantial portion of
    the contents of the database;
    b.if you include all or a substantial portion of the database contents
    in a database in which you have sui generis database rights, then the
    database in which you have sui generis database rights (but not its
    individual contents) is adapted material; and
    c.you must comply with the conditions in section 3(a) if you share all
    or a substantial portion of the contents of the database.
 .
 for the avoidance of doubt, this section 4 supplements and does not
 replace your obligations under this public license where the licensed
 rights include other copyright and similar rights.
 .
 Section 5 – Disclaimer of warranties and limitation of liability.
 .
    a.unless otherwise separately undertaken by the licensor, to the
    extent possible, the licensor offers the licensed material as-is and
    as-available, and makes no representations or warranties of any kind
    concerning the licensed material, whether express, implied, statutory,
    or other. this includes, without limitation, warranties of title,
    merchantability, fitness for a particular purpose, non-infringement,
    absence of latent or other defects, accuracy, or the presence or
    absence of errors, whether or not known or discoverable. where
    disclaimers of warranties are not allowed in full or in part, this
    disclaimer may not apply to you.
    b.to the extent possible, in no event will the licensor be liable to
    you on any legal theory (including, without limitation, negligence) or
    otherwise for any direct, special, indirect, incidental,
    consequential, punitive, exemplary, or other losses, costs, expenses,
    or damages arising out of this public license or use of the licensed
    material, even if the licensor has been advised of the possibility of
    such losses, costs, expenses, or damages. where a limitation of
    liability is not allowed in full or in part, this limitation may not
    apply to you.
 .
    c.the disclaimer of warranties and limitation of liability provided
    above shall be interpreted in a manner that, to the extent possible,
    most closely approximates an absolute disclaimer and waiver of all
    liability.
 .
 Section 6 – Term and termination.
 .
    a.this public license applies for the term of the copyright and
    similar rights licensed here. however, if you fail to comply with this
    public license, then your rights under this public license terminate
    automatically.
 .
    b.where your right to use the licensed material has terminated under
    section 6(a), it reinstates:
    1.automatically as of the date the violation is cured, provided it
    is cured within 30 days of your discovery of the violation; or
        2. upon express reinstatement by the licensor.
    c.for the avoidance of doubt, this section 6(b) does not affect any
    right the licensor may have to seek remedies for your violations of
    this public license.
    d.sections 1, 5, 6, 7, and 8 survive termination of this public license.
 .
 Section 7 – Other terms and conditions.
 .
    a.the licensor shall not be bound by any additional or different terms
    or conditions communicated by you unless expressly agreed.
    b.Any arrangements, understandings, or agreements regarding the
    Licensed Material not stated herein are separate from and independent
    of the terms and conditions of this Public License.
 .
 Section 8 – Interpretation.
 .
    a.For the avoidance of doubt, this Public License does not, and shall
    not be interpreted to, reduce, limit, restrict, or impose conditions
    on any use of the Licensed Material that could lawfully be made
    without permission under this Public License.
    b.To the extent possible, if any provision of this Public License is
    deemed unenforceable, it shall be automatically reformed to the
    minimum extent necessary to make it enforceable. If the provision
    cannot be reformed, it shall be severed from this Public License
    without affecting the enforceability of the remaining terms and
    conditions.
    c.No term or condition of this Public License will be waived and no
    failure to comply consented to unless expressly agreed to by the
    Licensor.
    d.Nothing in this Public License constitutes or may be interpreted as
    a limitation upon, or waiver of, any privileges and immunities that
    apply to the Licensor or You, including from the legal processes of
    any jurisdiction or authority.
 .
 Creative Commons is not a party to its public licenses. Notwithstanding,
 Creative Commons may elect to apply one of its public licenses to
 material it publishes and in those instances will be considered the
 “Licensor.” The text of the Creative Commons public licenses is dedicated
 to the public domain under the CC0 Public Domain Dedication. Except for
 the limited purpose of indicating that material is shared under a
 Creative Commons public license or as otherwise permitted by the Creative
 Commons policies published at creativecommons.org/policies, Creative
 Commons does not authorize the use of the trademark “Creative Commons” or
 any other trademark or logo of Creative Commons without its prior written
 consent including, without limitation, in connection with any
 unauthorized modifications to any of its public licenses or any other
 arrangements, understandings, or agreements concerning use of licensed
 material. For the avoidance of doubt, this paragraph does not form part
 of the public licenses.
